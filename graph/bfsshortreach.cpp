/*
Breadth First Search: Shortest Reach
Challenge link: https://www.hackerrank.com/challenges/bfsshortreach
Profile link: https://www.hackerrank.com/enki97
*/

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

//Graph Data Structure
struct graph_t{
    bool **matrix;
    int vertices;
};

typedef graph_t *graph;

graph create_graph(int vertices){
    graph g = new graph_t;
    g->matrix = new bool*[vertices + 1];
    for(int i = 0; i <= vertices; i++)
        g->matrix[i] = new bool[vertices + 1];

    for(int i = 1; i <= vertices; i++)
        for(int j = 1; j <= vertices; j++)
            g->matrix[i][j] = 0;
    g->vertices = vertices;
    return g;
}

//Adds undirected edge
void add_edge(graph &g, int v1, int v2){
    g->matrix[v1][v2] = 1;
    g->matrix[v2][v1] = 1;
}

void destroy_edge(graph &g, int v1, int v2){
    g->matrix[v1][v2] = 0;
}

void destroy_graph(graph &g){
    if(g != NULL){
        for(int i = 0; i < g->vertices; i++)
            delete[] g->matrix[i];
        delete[] g->matrix;
    }
}

typedef bool *arr_bool;

void dfs(graph g, arr_bool &visited, int s){
    for(int i = 1; i<=g->vertices; i++){
        if(g->matrix[s][i] && !visited[i]){
            visited[i] = 1;
            dfs(g, visited, i);
        }
    }
}

arr_bool connected_component(graph g, int s){
    arr_bool res = new bool[g->vertices + 1];
    for(int i = 0; i <= g->vertices; i++)
        res[i] = 0;
    res[s]=1;
    dfs(g, res, s);
    return res;
}

bool equal(int n, arr_bool a1, arr_bool a2){
    int i = 0;
    while(i < n && a1[i] == a2[i])
        i++;
    return i == n;
}

bool is_inf(int v){
    return v==-1;
}

int select(int n, int *distance, arr_bool visited){
    int min = -1;
    int pos = 0;
    int i = 1;
    while(i < n){
        if(((distance[i] < min && !is_inf(distance[i])) || (!is_inf(distance[i]) && is_inf(min))) && !visited[i]){
            min = distance[i];
            pos = i;
        }
        i++;
    }
    return pos;
}

int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */
    int queries, vertices, edges, s;
    arr_bool visited, total;
    int *distance;
    graph g;

    cin >> queries;
    while(queries > 0){
        cin >> vertices;
        cin >> edges;

        g = create_graph(vertices);

        for(int i = 0; i < edges; i++){
            int v1, v2;
            cin >> v1;
            cin >> v2;

            add_edge(g, v1, v2);
        }
        cin >> s;

        total = connected_component(g, s);

        visited = new bool[vertices + 1];
        for(int i = 0; i <= vertices; i++)
            visited[i] = 0;
        visited[s] = 1;

        distance = new int[vertices + 1];
        for(int i = 0; i <= vertices; i++){
            if(g->matrix[s][i])
                distance[i] = 6;
            else
                distance[i] = -1;
        }

        while(!equal(vertices + 1, visited, total)){
            int v = select(vertices + 1, distance, visited);
            visited[v] = 1;
            for(int i = 1; i <= vertices; i++){
                if(g->matrix[v][i] && (distance[v] + 6 < distance[i] || (is_inf(distance[i]) && i!=s)))
                    distance[i] = distance[v] + 6;
            }
        }

        for(int i = 1; i <= vertices; i++){
            if(i != s)
                cout << distance[i] << " ";
        }

        cout << endl;

        destroy_graph(g);
        delete [] visited;
        delete [] total;
        delete [] distance;

        queries--;
    }
    return 0;
}
